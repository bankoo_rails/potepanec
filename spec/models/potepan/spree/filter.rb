require 'rails_helper'

RSpec.describe Spree::Filter, type: :model do
  let!(:taxon)             { create(:taxon) }
  let!(:taxon_product)     { create(:product, taxons: [taxon]) }
  let!(:non_taxon_product) { create(:product) }
  let!(:value_product)     { create(:product, taxons: [taxon]) }
  let!(:variant)           { create(:variant, product: value_product, option_values: [value]) }
  let!(:value)             { create(:option_value) }

  describe 'filtered_productsメソッド' do
    context '@taxonあり@option_valueありの場合' do
      it "２重にフィルターされたproductが得られる" do
        filter = Spree::Filter.new(taxon: taxon, option_value: value)
        expect(filter.filtered_products).to match_array(value_product)
      end
    end

    context '@taxonあり@option_valueなしの場合' do
      it "@taxonでフィルターされたproductが得られる" do
        filter = Spree::Filter.new(taxon: taxon, option_value: nil)
        products_associated_taxon = [taxon_product, value_product]
        expect(filter.filtered_products).to match_array(products_associated_taxon)
      end
    end

    context '@taxonなし@option_valueありの場合' do
      it "@option_valueでフィルターされた全てのproductが得られる" do
        filter = Spree::Filter.new(taxon: nil, option_value: value)
        expect(filter.filtered_products).to match_array(value_product)
      end
    end

    context '@taxonなし@option_valueなしの場合' do
      it "全てのproductが得られる" do
        filter = Spree::Filter.new(taxon: nil, option_value: nil)
        all_products = [taxon_product, non_taxon_product, value_product]
        expect(filter.filtered_products).to match_array(all_products)
      end
    end
  end
end
