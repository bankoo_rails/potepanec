require 'rails_helper'

RSpec.describe Spree::Taxon, type: :model do
  let!(:taxon)     { create(:taxon, name: 'taxon') }
  let!(:sub_taxon) { create(:taxon, name: 'sub_taxon') }

  describe 'search_by_namesメソッド' do
    it "該当するtaxonが得られること" do
      sample_array = ['taxon', 'sub_taxon']
      expect(Spree::Taxon.search_by_names(sample_array)).to eq [taxon, sub_taxon]
    end
  end
end
