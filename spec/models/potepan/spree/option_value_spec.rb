require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let!(:product)      { create(:product, master: variant) }
  let!(:sub_product)  { create(:product, master: sub_variant) }
  let!(:variant)      { create(:variant, option_values: [option_value]) }
  let!(:sub_variant)  { create(:variant, option_values: [option_value]) }
  let!(:option_value) { create(:option_value) }

  describe 'search_productsメソッド' do
    it "該当するproductが得られること" do
      expect(option_value.search_products).to match_array([product, sub_product])
    end
  end

  describe 'count_productsメソッド' do
    it "該当する個数(2)が得られること" do
      expect(option_value.count_products).to eq 2
    end
  end
end
