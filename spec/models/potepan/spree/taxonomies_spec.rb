require 'rails_helper'

RSpec.describe Spree::Taxonomy, type: :model do
  let!(:taxonomy)    { create(:taxonomy) }
  let!(:taxon)       { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:sub_taxon)   { create(:taxon, taxonomy: taxonomy) }

  describe 'taxons_listメソッド' do
    it "適切なtaxonが得られること" do
      expect(taxonomy.taxons_list).to match_array(taxon)
    end
  end
end
