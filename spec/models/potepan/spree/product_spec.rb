require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let!(:taxon)            { create(:taxon) }
  let!(:product)          { create(:product, taxons: [taxon], option_types: [option_type]) }
  let!(:same_taxons)      { create(:product, taxons: [taxon]) }
  let!(:different_taxons) { create(:product) }
  let!(:variant)          { create(:variant, product: product, option_values: [option_value]) }
  let!(:option_type)      { create(:option_type) }
  let!(:option_value)     { create(:option_value) }

  describe 'search_by_option_valueメソッド' do
    it "該当するproductが得られること" do
      expect(Spree::Product.search_by_option_value(option_value)).to match_array(product)
    end
  end

  describe 'related_productsメソッド' do
    it "関連商品のみが得られること" do
      expect(product.related_products).to match_array(same_taxons)
    end
  end
end
