require 'rails_helper'

RSpec.describe Spree::OptionType, type: :model do
  let!(:tshirt_color)  { create(:option_type, name: "tshirt-color") }
  let!(:tshirt_size)   { create(:option_type, name: "tshirt-size") }

  describe 'tshit_colorメソッド' do
    it "該当するoption_typeが得られること" do
      expect(Spree::OptionType.tshit_color).to eq tshirt_color
    end
  end

  describe 'tshit_sizeメソッド' do
    it "該当するoption_typeが得られること" do
      expect(Spree::OptionType.tshit_size).to eq tshirt_size
    end
  end
end
