require 'rails_helper'

RSpec.feature "Home", type: :feature do
  let!(:taxon)             { create(:taxon) }
  let!(:popular_taxon)     { create(:taxon, name: 'Clothing') }
  let!(:non_popular_taxon) { create(:taxon, name: 'Shirts') }
  let!(:new_product)       { create(:product, available_on: 1.month.ago, taxons: [taxon]) }
  let!(:non_new_product)   { create(:product, available_on: 2.month.ago, taxons: [taxon]) }
  let!(:user)              { create(:user, email: 'foobar@example.com', password: 'foobar') }

  feature "rootにアクセスする" do
    before do
      visit potepan_path
    end

    scenario "タイトルにサイト名が表示されること" do
      expect(page).to have_title("Potepanec")
    end
    scenario "タイトルロゴをクリックする" do
      within ".navbar-header" do
        click_on 'brand-logo'
      end
      expect(page).to have_current_path potepan_path
    end
    scenario "ヘッダーのHomeをクリックする" do
      within ".navbar-right" do
        click_on 'Home'
      end
      expect(page).to have_current_path potepan_path
    end
    scenario "人気カテゴリー(Clothing)が表示される" do
      within ".featuredCollection" do
        click_on popular_taxon.name
      end
      expect(page).to have_current_path potepan_category_path(popular_taxon.id)
    end
    scenario "人気カテゴリー(Clothing)以外は表示されない" do
      within ".featuredCollection" do
        expect(page).not_to have_content non_popular_taxon.name
      end
    end
    scenario "新着商品をクリックする" do
      within ".featuredProducts" do
        click_on new_product.name
      end
      expect(page).to have_current_path potepan_product_path(new_product.id)
    end
    scenario "新着商品以外は表示されない" do
      within ".featuredProducts" do
        expect(page).not_to have_content non_new_product.name
      end
    end
    scenario "モーダルから登録ユーザーがログインする" do
      within ".login-modal" do
        fill_in 'spree_user_email', with: 'foobar@example.com'
        fill_in 'spree_user_password', with: 'foobar'
        click_on "ログイン"
      end
      expect(page).to have_content "ログアウト"
    end
    scenario "モーダルから新規ユーザーがサインインする" do
      within "#signup" do
        fill_in 'spree_user_email', with: 'newuser@example.com'
        fill_in 'spree_user_password', with: 'newuser'
        fill_in 'spree_user_password_confirmation', with: 'newuser'
        click_on "登録する"
      end
      expect(page).to have_content "ログアウト"
    end
  end
end
