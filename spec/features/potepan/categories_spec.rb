require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:taxonomy)     { create(:taxonomy) }
  let!(:taxon)        { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:product)      { create(:product, taxons: [taxon]) }
  let!(:sub_product)  { create(:product, taxons: [taxon]) }
  let!(:variant)      { create(:variant, product: product, option_values: [color, size]) }
  let!(:tshirt_color) { create(:option_type, name: "tshirt-color") }
  let!(:tshirt_size)  { create(:option_type, name: "tshirt-size") }
  let!(:color)        { create(:option_value, option_type: tshirt_color) }
  let!(:size)         { create(:option_value, option_type: tshirt_size) }

  feature "categories/showにアクセスする" do
    before do
      visit potepan_category_path(id: taxon.id)
    end

    scenario "カテゴリ名＋サイト名のタイトルが表示される" do
      expect(page).to have_title("#{taxon.name} | Potepanec")
    end
    scenario "パンくずリストのHomeをクリックする" do
      within ".breadcrumb" do
        click_on "Home"
      end
      expect(page).to have_current_path potepan_path
    end
    scenario "CATEGORIESの商品名をクリックする" do
      within ".side-nav" do
        expect(page).to have_content taxonomy.name
        expect(page).to have_content "#{taxon.name} (#{taxon.all_products.count})"
        click_on taxon.name
      end
      expect(page).to have_current_path potepan_category_path(taxon.id)
    end
    scenario "FILTER BY COLORのカラー名をクリックする" do
      expect(page).to have_content tshirt_color.presentation
      expect(page).to have_content "#{color.name}(#{color.count_products})"
      click_on color.name
      expect(page).to have_current_path potepan_products_path(option_value: color)
    end
    scenario "FILTER BY SIZEのサイズ名をクリックする" do
      expect(page).to have_content tshirt_size.presentation
      expect(page).to have_content "#{size.name}(#{size.count_products})"
      click_on size.name
      expect(page).to have_current_path potepan_products_path(option_value: size)
    end
    scenario "商品名の1つをクリックする" do
      within "##{product.slug}" do
        expect(page).to have_link product.display_price.to_s, href: potepan_product_path(product.id)
        click_on product.name
      end
      expect(page).to have_current_path potepan_product_path(product.id)
    end
    scenario "商品画像クリックする" do
      within "##{product.slug}-image" do
        click_on product.display_image.attachment_file_name
      end
      expect(page).to have_current_path potepan_product_path(product.id)
    end
    scenario "商品表示を変更する" do
      click_on "list"
      expect(page).to have_css '.productListSingle'
    end
    scenario "LISTボタンをクリックする" do
      click_on "list"
      expect(page).to have_current_path potepan_category_path(taxon.id, display_type: "list")
    end
  end
end
