require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let!(:taxon)              { create(:taxon) }
  let!(:product)            { create(:product, taxons: [taxon], option_types: [option_type]) }
  let!(:taxons_product)     { create(:product, taxons: [taxon]) }
  let!(:non_taxons_product) { create(:product) }
  let!(:variant)            { create(:variant, product: product, option_values: [option_value]) }
  let!(:option_value)       { create(:option_value, option_type: option_type) }
  let!(:option_type)        { create(:option_type) }

  feature "products/indexにアクセスする" do
    before do
      visit potepan_products_path
    end

    scenario "タイトルにFilter Productsと表示される" do
      expect(page).to have_title("Filter Products | Potepanec")
    end
    scenario "パンくずリストのHomeをクリックする" do
      within ".breadcrumb" do
        click_on "Home"
      end
      expect(page).to have_current_path potepan_path
    end
    scenario "LISTボタンをクリックする" do
      click_on "list"
      expect(page).to have_current_path potepan_products_path(display_type: "list")
    end
  end

  feature "products/showにアクセスする" do
    before do
      visit potepan_product_path(id: product.id)
    end

    scenario "製品名＋サイト名のタイトルが表示される" do
      expect(page).to have_title("#{product.name} | Potepanec")
    end
    scenario "パンくずリストのHomeをクリックする" do
      within ".breadcrumb" do
        click_on "Home"
      end
      expect(page).to have_current_path potepan_path
    end
    scenario "基本的な製品情報が表示される" do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
    scenario "カテゴリーページへのリンクをクリックする" do
      expect(page).to have_link href: potepan_category_path(taxon.id)
    end
    scenario "関連商品をクリックする" do
      within ".productsContent" do
        click_on taxons_product.name
      end
      expect(page).to have_current_path potepan_product_path(taxons_product.id)
    end
    scenario "関連商品以外は表示されない" do
      within ".productsContent" do
        expect(page).not_to have_content non_taxons_product.name
      end
    end
  end
end
