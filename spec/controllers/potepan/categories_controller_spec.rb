require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    let!(:taxonomy)     { create(:taxonomy) }
    let!(:taxon)        { create(:taxon, taxonomy: taxonomy) }
    let!(:product)      { create(:product, taxons: [taxon]) }
    let!(:sub_product)  { create(:product) }
    let!(:tshirt_color) { create(:option_type, name: "tshirt-color") }
    let!(:tshirt_size)  { create(:option_type, name: "tshirt-size") }

    before do
      get :show, params: { id: taxon.id }
    end

    it "正常にレスポンスを返すこと" do
      expect(response).to be_successful
    end
    it "showテンプレートを返すこと" do
      expect(response).to render_template :show
    end
    it "＠taxonomiesがアサインされていること" do
      expect(assigns(:taxonomies)).to match_array(taxonomy)
    end
    it "＠search_colorがアサインされていること" do
      expect(assigns(:search_color)).to eq tshirt_color
    end
    it "＠search_sizeがアサインされていること" do
      expect(assigns(:search_size)).to eq tshirt_size
    end
    it "＠taxonがアサインされていること" do
      expect(assigns(:taxon)).to eq taxon
    end
    it "＠taxonに関連する＠productsのみがアサインされていること" do
      expect(assigns(:products)).to match_array(product)
    end
  end
end
