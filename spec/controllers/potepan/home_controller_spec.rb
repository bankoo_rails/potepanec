require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe "#index" do
    let!(:popular_taxon)     { create(:taxon, name: 'Clothing') }
    let!(:non_popular_taxon) { create(:taxon, name: 'Shirts') }
    let!(:new_product)       { create(:product, available_on: 1.month.ago) }
    let!(:non_new_product)   { create(:product, available_on: 2.month.ago) }

    before do
      get :index
    end

    it "正常にレスポンスを返すこと" do
      expect(response).to be_successful
    end
    it "indexテンプレートを返すこと" do
      expect(response).to render_template :index
    end
    it "＠popular_taxonsに人気カテゴリー(Clothing)だけがアサインされていること" do
      expect(assigns(:popular_taxons)).to match_array(popular_taxon)
    end
    it "＠new_productsに新規商品だけがアサインされていること" do
      expect(assigns(:new_products)).to match_array(new_product)
    end
  end
end
