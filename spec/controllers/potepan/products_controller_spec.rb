require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#index" do
    let!(:taxonomy)     { create(:taxonomy) }
    let!(:taxon)        { create(:taxon, taxonomy: taxonomy) }
    let!(:product)      { create(:product, taxons: [taxon]) }
    let!(:sub_product)  { create(:product) }
    let!(:tshirt_color) { create(:option_type, name: "tshirt-color") }
    let!(:tshirt_size)  { create(:option_type, name: "tshirt-size") }

    before do
      get :index
    end

    it "正常にレスポンスを返すこと" do
      expect(response).to be_successful
    end
    it "showテンプレートを返すこと" do
      expect(response).to render_template :index
    end
    it "＠taxonomiesがアサインされていること" do
      expect(assigns(:taxonomies)).to match_array(taxonomy)
    end
    it "＠search_colorがアサインされていること" do
      expect(assigns(:search_color)).to eq tshirt_color
    end
    it "＠search_sizeがアサインされていること" do
      expect(assigns(:search_size)).to eq tshirt_size
    end
    it "＠productsが適切にアサインされていること" do
      expect(assigns(:products)).to match_array([product, sub_product])
    end
  end

  describe "#show" do
    let!(:taxon)                { create(:taxon) }
    let!(:product)              { create(:product, taxons: [taxon]) }
    let!(:same_taxons_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it "正常にレスポンスを返すこと" do
      expect(response).to be_successful
    end
    it "showテンプレートを返すこと" do
      expect(response).to render_template :show
    end
    it "＠productがアサインされていること" do
      expect(assigns(:product)).to eq product
    end
    it "＠related_productsの要素が４つであること" do
      expect(assigns(:related_products).count).to eq 4
    end
  end
end
