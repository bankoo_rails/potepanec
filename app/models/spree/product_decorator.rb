Spree::Product.class_eval do
  scope :includes_price_image, -> { includes(master: [:default_price, :images]) }

  def self.search_by_option_value(option_value)
    joins(variants: :option_values).where(spree_option_values: { id: option_value })
  end

  def related_products
    if taxons.present?
      Spree::Product.in_taxons(taxons).where.not(id: id).distinct
    else
      Spree::Product.none
    end
  end
end
