class Spree::Filter
  attr_reader :taxon, :option_value, :display_type

  def initialize(params = {})
    @taxon        = params[:taxon].presence
    @option_value = params[:option_value].presence
    @display_type = params[:display_type] || 'grid'
  end

  def filtered_products
    scopes = base_scope(taxon: @taxon)
    scopes = scopes.search_by_option_value(@option_value) if @option_value
    scopes
  end

  def grid_display?
    display_type == 'grid'
  end

  private

  def base_scope(taxon: nil)
    scope = Spree::Product.includes_price_image.distinct
    taxon.present? ? scope.in_taxon(taxon) : scope
  end
end
