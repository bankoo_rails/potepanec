Spree::Taxonomy.class_eval do
  def taxons_list
    taxons = self.taxons
    if taxons
      taxons.select { |taxon| taxon.parent_id.present? }
    else
      []
    end
  end
end
