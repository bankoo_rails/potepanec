Spree::OptionType.class_eval do
  class << self
    def search_by_names(names_array)
      find_by(name: names_array)
    end

    def tshit_color
      search_by_names('tshirt-color')
    end

    def tshit_size
      search_by_names('tshirt-size')
    end
  end
end
