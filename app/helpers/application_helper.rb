module ApplicationHelper
  def full_title(page_title: '')
    base_title = "Potepanec"
    if page_title.blank?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  def resource_name
    :spree_user
  end

  def resource
    @resource ||= Spree::User.new
  end
end
