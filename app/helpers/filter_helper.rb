module FilterHelper
  def display_type
    params[:display_type] || 'grid'
  end

  def active?(type)
    'active' if type == display_type
  end

  def categories_controller?
    controller.controller_name == 'categories'
  end
end
