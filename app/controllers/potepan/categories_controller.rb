class Potepan::CategoriesController < ApplicationController
  before_action :set_sidebar, only: [:show]

  def show
    @taxon          = Spree::Taxon.find(params[:id])
    @product_filter = Spree::Filter.new(filter_params.merge(taxon: @taxon))
    @products       = @product_filter.filtered_products
  end

  private

  def filter_params
    params.permit(:option_value, :display_type)
  end

  def set_sidebar
    @taxonomies   = Spree::Taxonomy.includes(:taxons)
    @search_color = Spree::OptionType.tshit_color
    @search_size  = Spree::OptionType.tshit_size
  end
end
