class Potepan::HomeController < ApplicationController
  FIRST_DAY_OF_NEW_ARRIVAL_PRODUCTS = 1.month.ago                  # 暫定的に1ヶ月間を新着商品期間に設定
  MAX_NUMBER_OF_NEW_ARRIVAL_PRODUCTS_DISPLAY = 4                   # 暫定的に新着商品の表示個数を設定
  THREE_POPULAR_TAXONS = ['Categories', 'Clothing', 'Bags'].freeze # 暫定的に人気商品を３つ設定

  def index
    @popular_taxons = Spree::Taxon.search_by_names(THREE_POPULAR_TAXONS)
    @new_products   = Spree::Product.where('available_on >= ?', FIRST_DAY_OF_NEW_ARRIVAL_PRODUCTS).
      includes(master: :default_price).sample(MAX_NUMBER_OF_NEW_ARRIVAL_PRODUCTS_DISPLAY)
  end
end
