class Potepan::ProductsController < ApplicationController
  before_action :set_sidebar, only: [:index]
  MAX_NUMBER_OF_DISPLAY_PRODUCTS = 4

  def index
    @product_filter = Spree::Filter.new(filter_params)
    @products       = @product_filter.filtered_products
  end

  def show
    @product = Spree::Product.find(params[:id])
    @related_products =
      @product.related_products.
        includes_price_image.
        sample(MAX_NUMBER_OF_DISPLAY_PRODUCTS)
  end

  private

  def filter_params
    params.permit(:option_value, :display_type)
  end

  def set_sidebar
    @taxonomies   = Spree::Taxonomy.includes(:taxons)
    @search_color = Spree::OptionType.tshit_color
    @search_size  = Spree::OptionType.tshit_size
  end
end
