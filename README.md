# Solidusを拡張したECサイト

このアプリはRuby on Rails製のOSSである、[Solidus](https://solidus.io/)を拡張したサンプルECサイトです。  
ユーザー画面では商品の閲覧、管理者画面では商品の登録が可能です。  
https://bankooec.herokuapp.com/potepan #ユーザー画面  
https://bankooec.herokuapp.com/admin/login #管理者画面(email: admin@example.com pass: test123)  

## 機能一覧
 - Dockerを用いたRails環境構築(Docker)
 - Rails製OSSのSolidusのキャッチアップ(Solidus)
 - モデル、コントローラー、システムテスト(Rspec, capybara)
 - インフラ(Heroku, Doker)
 - データベース(mysql, postgreSQL)
 - リンターの導入(rubocop-airbnb)
 - N+1問題を考慮したSQLクエリの発行(bullet)
 - 商品詳細ページの機能
 - カテゴリー別アーカイブページの機能
 - 関連商品出力機能

## 開発環境のセットアップ
### homebrew のインストール
```bash
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
### Docker のインストール
```bash
brew update
brew install caskroom/cask/brew-cask
brew cask install docker
```
### Docker の起動
```bash
open /Applications/Docker.app
```
### 動作開始
```bash
docker-compose up --build
```
### 動作停止
```bash
docker-compose stop
docker-compose down
```
